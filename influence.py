# - * - coding: UTF-8 - * -

import time
import os
from selenium import webdriver
from random import randint
import json
from openpyxl import Workbook
from bs4 import BeautifulSoup


class Browser:

    def __init__(self,headless=False):
        try:
             executable_path = os.path.join(os.getcwd(), 'chromedriver')
             print(executable_path)
             options = webdriver.ChromeOptions()
             if headless:
                options.add_argument('headless')
             options.add_argument('window-size=1200x600')
             self.browser = webdriver.Chrome(executable_path=executable_path, chrome_options=options)
             self.browser.set_page_load_timeout(100)
             self.browser.set_script_timeout(100)
        except Exception as exp:
            print(str(exp))
            print("Webdriver open failed.")

    def OpenURL(self,url):
        try:
            self.browser.get(url)
            self.browser.implicitly_wait(10)
            # time.sleep(10)

            return True
        except Exception as exp:
            return False

    def GetPage(self):
        try:
            self.page = self.browser.page_source
            encodedStr = self.page.encode("ascii","xmlcharrefreplace")
            return encodedStr
        except Exception as exp:
            return None

    def HitEnter(self, element):
        try:
            element.send_keys(u'\ue007')
        except Exception as exp:
            print(str(exp))

    def ClickElement(self,element):
        try:
            if element is not None:
                try:
                    element.click()
                    time.sleep(10)
                    return True
                except Exception as e:
                    print("Click Exception: "+str(e))
                    return False
        except Exception as exp:
            return False

    def TypeInto(self,text,element):
        try:
            element.send_keys(text)
        except Exception as exp:
            print(str(exp))

    def ClearText(self,element):
        try:
            element.clear()
        except Exception as exp:
            print('Text Clear Failed!')
            pass

    def FindElementByName(self,elementName):
        try:
            element = self.browser.find_element_by_name(elementName)
            return element
        except Exception as exp:
            return None

    def FindElementById(self,elementId):
        try:
            element = self.browser.find_element_by_id(elementId)
            return element
        except Exception as exp:
            return None

    def FindElementByClassName(self,elementClassName):
        try:
            element = self.browser.find_element_by_css_selector(elementClassName)
            return element
        except Exception as exp:
            return None

    def FindElementByText(self,text):
        try:
            element = self.browser.find_element_by_link_text(text)
            return element
        except Exception as exp:
            return None

    def ExecuteScriptAndWait(self,code):
        try:
            self.browser.execute_script(code)
            time.sleep(7)
        except Exception as exp:
            pass

    def GetPageURL(self):
        try:
            return self.browser.current_url
        except Exception as exp:
            return None

    def Close(self):
        try:
            self.browser.close()
        except Exception as exp:
            print("Browser closing failed.")

    def scroll_to_pager_link(self):
        try:
            scrollto_pager_link = 'var el = document.getElementById("Y-N-ffs");el.scrollIntoView(true);'
            #scrollto_pager_link = 'var el = document.getElementsByClassName("pagination");el[0].scrollIntoView(true);'
            self.ExecuteScriptAndWait(scrollto_pager_link)
        except Exception as exp:
            print("Exception Inside page scroll. %s" % str(exp))

    def find_element_by_xpath(self, element_type, type_value):
        try:
            element = self.browser.find_element_by_xpath("//%s[@type='%s']" % (element_type, type_value))
        except Exception as exp:
            print(str(exp))

    def find_element_contains_text(self, text):
        try:
            elements = self.browser.find_elements_by_xpath("//*[contains(text(), '%s')]" % text)
            if elements:
                return elements[0]
        except Exception as exp:
            pass


class Scraper(object):
    def __init__(self, page):
        self.page = page
        self.soup = BeautifulSoup(self.page, features="html.parser")

    def extract_profile_links(self):
        profile_urls = []
        anchors = self.soup.findAll("a", {"class": "advanced-search-card"})
        for a in anchors:
            profile_urls += [a.get('href')]

        cards = self.soup.findAll('div', {'class': 'influencer-card'})
        for card in cards:
            anchors = card.findAll('a')
            if anchors:
                profile_urls += [anchors[0].get('href')]

        return profile_urls

    def element_contains_text(self, element, text):
        elements = self.soup.findAll(element, text=text)
        return elements

    def text_to_numeric(self, text):
        try:
            text = text.replace('\n', '').strip()
            if text.endswith('k'):
                return float(text.replace('k', '').strip()) * 1000
            elif text.endswith('m'):
                return float(text.replace('k', '').strip()) * 1000000
            else:
                return float(text)
        except Exception as exp:
            return text

    def parse_details(self):
        h1 = self.soup.find('h1')
        details = {
            'name': '',
            'reach': '',
            'location': ''
        }
        if h1:
            details['name'] = h1.text
        reach_elements = self.soup.findAll('p', {'class': 'emojify-me'})
        if reach_elements:
            strong_el = reach_elements[0].findAll('strong')
            if strong_el and len(strong_el) > 1:
                reach_count = strong_el[1].text
                details['reach'] = self.text_to_numeric(reach_count)

        # Find out the location
        location_els = self.soup.find_all("strong", string="Location")
        if location_els:
            location_text = location_els[0].parent.text
            location_text = location_text.replace('Location', '').replace('\n', '').strip()
            details['location'] = location_text
        else:
            details['location'] = ''

        network_elements = self.soup.findAll('li', {'class': 'network'})
        for network_element in network_elements:
            anchor_elements = network_element.findAll('a')
            # print(anchor_elements)
            if anchor_elements:
                link = anchor_elements[0].get('href') or ''
                # print(link)
                text = anchor_elements[0].text or ''
                badge_influence = network_element.findAll('span', {'class': 'badge-influence'})
                if badge_influence:
                    badge_number = badge_influence[0].text
                else:
                    badge_number = ''
                if badge_number:
                    badge_number = self.text_to_numeric(badge_number)
                if 'youtube' in link:
                    details['youtube'] = {
                        'link': link,
                        'name': text,
                        'badge_number': badge_number
                    }
                elif 'influence' in link:
                    details['influence'] = {
                        'link': link,
                        'name': text,
                        'badge_number': badge_number
                    }
                elif 'linkedin' in link:
                    details['linkedin'] = {
                        'link': link,
                        'name': text,
                        'badge_number': badge_number
                    }
                elif 'instagram' in link:
                    details['instagram'] = {
                        'link': link,
                        'name': text,
                        'badge_number': badge_number
                    }
                elif 'facebook' in link:
                    details['facebook'] = {
                        'link': link,
                        'name': text,
                        'badge_number': badge_number
                    }
                elif 'twitter' in link:
                    details['twitter'] = {
                        'link': link,
                        'name': text,
                        'badge_number': badge_number
                    }
                elif 'pinterest' in link:
                    details['pinterest'] = {
                        'link': link,
                        'name': text,
                        'badge_number': badge_number
                    }

        # Find Website
        website_els = self.soup.find_all("strong", string="Website")
        if website_els:
            p = website_els[0].parent
            if p:
                website_anchors = p.findAll('a')
                if website_anchors:
                    details['website'] = website_anchors[0].get('href')
                else:
                    details['website'] = ''
            else:
                details['website'] = ''
        else:
            details['website'] = ''

        # Top Country and post starting rate
        details['top_country'] = ''
        details['top_country_percentage'] = ''
        details['post_starting_rate'] = ''
        about_contents = self.soup.findAll('div', {'class': 'influencer-about-content'})
        if about_contents and len(about_contents) == 2:

            about_content1 = about_contents[0]
            h4s = about_content1.findChildren('h4', recursive=False)
            for hh4 in h4s:
                text = hh4.text.strip()
                print(text)
                if 'Posts Starting at' in text:
                    rate_value = text.replace('Posts Starting at', '').strip()
                    details['post_starting_rate'] = rate_value

            about_content = about_contents[1]
            headings = about_content.findChildren('h4', recursive=False)
            rows = about_content.findChildren('div', {'class': 'row'}, recursive=False)
            for i, heading in enumerate(headings):
                heading_text = heading.text.replace('\n', '').strip()
                if heading_text == 'Top Country':
                    row = rows[i]
                    name_el = row.findAll('div', {'class': 'col-xs-9'})
                    if name_el:
                        country_name = name_el[0].text.replace('\n', '').strip()
                        details['top_country'] = country_name or ''
                    percentage = row.findAll('div', {'class': 'col-xs-3'})
                    if percentage:
                        percentage = percentage[0].text.replace('\n', '').strip()
                        details['top_country_percentage'] = percentage or ''

        # Find engagement rate, likes per post, comments per post
        details['engagement_rate'] = ''
        details['likes_ppost'] = ''
        details['comments_ppost'] = ''

        chart_js_el = self.soup.findAll('div', {'class': 'chart-wrapper'})
        if chart_js_el:
            engagement_rate = chart_js_el[0].findChildren('p', recursive=False)
            if engagement_rate:
                details['engagement_rate'] = engagement_rate[0].text.replace('\n', '').strip()

        number_els = self.soup.findAll('div', {'class': 'number'})
        for number_el in number_els:
            txt = number_el.text.strip()
            if 'Likes per Post' in txt:
                details['likes_ppost'] = txt.replace('Likes per Post', '').strip()

            elif 'Comments per Post' in txt:
                details['comments_ppost'] = txt.replace('Comments per Post', '').strip()

        return details


def save_excel(file_name, header, data):
    book = Workbook()
    sheet = book.active

    for i, heading in enumerate(header):
        sheet.cell(row=1, column=i + 1).value = heading

    for row in data:
        sheet.append(row)

    book.save(file_name)


def perform_influence_login(browser_instance, username, password):
    SITE_LOGIN_URL = "https://influence.co/users/auth/instagram"
    browser_instance.OpenURL(SITE_LOGIN_URL)
    time.sleep(10)
    username_element = browser_instance.FindElementByName("username")
    password_element = browser_instance.FindElementByName("password")

    browser_instance.TypeInto(username, username_element)
    browser_instance.TypeInto(password, password_element)

    browser_instance.HitEnter(password_element)


if __name__ == "__main__":

    SITE_URL = "https://influence.co/"
    SEARCH_URL = "https://influence.co/influencer_searches/advanced?utf8=%E2%9C%93&is%5Blimit%5D=30&is%5Bsort_order%5D=reach_desc&is%5Bcategory_ids%5D%5B%5D=&is%5Bcategory_ids%5D%5B%5D=56d8b9064a047d478b00000a&is%5Blocation_ids%5D%5B%5D=&is%5Blocation_ids%5D%5B%5D=country-US&is%5Blocation_ids%5D%5B%5D=country-CA&is%5Bfollower_min%5D=&is%5Bfollower_max%5D=&is%5Bmin_fb_likes%5D=&is%5Bmax_fb_likes%5D=&is%5Bmin_twitter_followers%5D=&is%5Bmax_twitter_followers%5D=&is%5Bmin_erate%5D=1&is%5Bmax_erate%5D=&is%5Bmin_ga_visitors%5D=&is%5Bmax_ga_visitors%5D=&is%5Bmin_pinterest_followers%5D=&is%5Bmax_pinterest_followers%5D=&is%5Bmin_age%5D=&is%5Bmax_age%5D=&is%5Bmin_rate%5D=&is%5Bmax_rate%5D=&is%5Bis_male%5D=0&is%5Bis_female%5D=0&is%5Bhas_media_kit%5D=0&is%5Bis_claimed%5D=0&is%5Bhas_ig_posts%5D=0&is%5Bhas_yt_posts%5D=0&is%5Bhas_pinterest_posts%5D=0&is%5Bhas_snapchat_posts%5D=0&is%5Bhas_google_analytics%5D=0&is%5Baccount_type%5D=influencer&is%5Bsearch_type%5D=advanced&is%5Bmin_reach%5D=2000&is%5Bmax_reach%5D=10000000&commit=Search#search-results"
    LOGIN_USERNAME = "aacook"
    LOGIN_PASSWORD = "6nfpr6mhgKHGnshTf"
    print("Scraping: %s" % SITE_URL)
    browser = Browser(headless=False)

    perform_influence_login(browser_instance=browser, username=LOGIN_USERNAME,
                            password=LOGIN_PASSWORD)

    rtime = randint(5, 10)
    time.sleep(rtime)

    browser.OpenURL(SEARCH_URL)

    more_link = browser.find_element_contains_text("More Results")

    while more_link is not None:
        rtime = randint(4, 7)
        time.sleep(rtime)
        more_link.click()
        rtime = randint(2, 10)
        time.sleep(rtime)
        more_link = browser.find_element_contains_text("More Results")
        if more_link is None:
            rtime = randint(8, 10)
            time.sleep(rtime)
            more_link = browser.find_element_contains_text("More Results")

    page = browser.GetPage()

    scraper = Scraper(page)
    profile_links = scraper.extract_profile_links()

    d = {}
    for pl in profile_links:
        d[pl] = False

    with open('profiles.json', 'w') as outfile:
        json.dump(d, outfile)

    print("Profile Links extraction done...")

    # print("Closing browser")

    # browser.Close()

    profile_links_dict = {}
    with open('profiles.json', 'r') as jf:
        content = jf.read()
        profile_links_dict = json.loads(content)

    print("%s records found" % len(profile_links_dict.values()))

    # browser = Browser(headless=False)
    # perform_influence_login(browser_instance=browser, username=LOGIN_USERNAME,
    #                         password=LOGIN_PASSWORD)

    for link, profile in profile_links_dict.items():
        if not profile or profile == 'false':
            draw = randint(0, 1)
            if draw:
                rtime = randint(20, 50)
                time.sleep(rtime)

            browser.OpenURL('https://influence.co' + link)

            time.sleep(2)

            page = browser.GetPage()

            i = 0

            while i < 3:
                if page:
                    scraper = Scraper(page=page)

                    login_btns = scraper.element_contains_text('a', 'LOG IN')

                    if login_btns:
                        perform_influence_login(browser_instance=browser,
                                                username=LOGIN_USERNAME,
                                                password=LOGIN_PASSWORD)
                    else:
                        break

                page = browser.GetPage()

                i += 1

                time.sleep(5)

            page = browser.GetPage()
            scraper = Scraper(page=page)
            profile_details = scraper.parse_details()
            profile_links_dict[link] = profile_details
            with open('profiles.json', 'w') as file:
                json.dump(profile_links_dict, file)
            rtime = randint(3, 10)
        else:
            continue

    # Now save the data in excel

    profile_links_dict = {}
    with open('profiles.json', 'r') as jf:
        content = jf.read()
        profile_links_dict = json.loads(content)

    file_name = 'influence.xlsx'

    header = (
        'URL', 'Name', "Locations", 'Total Reach', 'Youtube Link', 'Youtube', 'Instagram Link',
        'Facebook Link', 'facebook', 'Twitter Link', 'Twitter', 'Pinterest Link', 'Pinterest',
        'Website URL', 'Top Country', 'Top Country %', 'Likes Per Post', 'Comments Per Post',
        'Starting Rate', 'Instagram', '% Engagement'
    )

    data = []

    for link, profile in profile_links_dict.items():
        if profile and profile != 'false':
            single_profile = ['https://influence.co'+link]
            single_profile += [profile['name']]
            single_profile += [profile['location']]
            single_profile += [profile['reach']]
            if profile.get('youtube'):
                single_profile += [profile['youtube']['link']]
                single_profile += [profile['youtube']['badge_number']]
            else:
                single_profile += ['']
                single_profile += ['']

            if profile.get('instagram'):
                single_profile += [profile['instagram']['link']]
            else:
                single_profile += ['']

            if profile.get('facebook'):
                single_profile += [profile['facebook']['link']]
                single_profile += [profile['facebook']['badge_number']]
            else:
                single_profile += ['']
                single_profile += ['']

            if profile.get('twitter'):
                single_profile += [profile['twitter']['link']]
                single_profile += [profile['twitter']['badge_number']]
            else:
                single_profile += ['']
                single_profile += ['']

            if profile.get('pinterest'):
                single_profile += [profile['pinterest']['link']]
                single_profile += [profile['pinterest']['badge_number']]
            else:
                single_profile += ['']
                single_profile += ['']

            single_profile += [profile['website']]

            single_profile += [profile['top_country']]
            single_profile += [profile['top_country_percentage']]

            single_profile += [profile['likes_ppost']]
            single_profile += [profile['comments_ppost']]

            single_profile += [profile['post_starting_rate']]

            if profile.get('instagram'):
                single_profile += [profile['instagram']['badge_number']]
            else:
                single_profile += ['']

            single_profile += [profile['engagement_rate'].replace('Engagement', '').replace('Rate', '').strip()]

            data += [tuple(single_profile)]

    save_excel(file_name=file_name, header=header, data=data)





