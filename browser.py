# - * - coding: UTF-8 - * -

import time
import os
from selenium import webdriver


class Browser:

    def __init__(self,proxy_ip=None):
        try:
             options = webdriver.ChromeOptions()
             options.add_argument('headless')
             options.add_argument('window-size=1200x600')
             executable_path = os.path.join(os.path.dirname(__file__), 'chromedriver')
             self.browser = webdriver.Chrome(executable_path=executable_path, chrome_options=options)
             self.browser.set_page_load_timeout(100)
             self.browser.set_script_timeout(100)
        except Exception as exp:
            print(str(exp))
            print("Webdriver open failed.")

    def OpenURL(self,url):
        try:
            self.browser.get(url)
            self.browser.implicitly_wait(20)
            time.sleep(10)

            return True
        except Exception as exp:
            return False

    def GetPage(self):
        try:
            self.page = self.browser.page_source
            encodedStr = self.page.encode("ascii","xmlcharrefreplace")
            return encodedStr
        except Exception as exp:
            return None
    def ClickElement(self,element):
        try:
            if element is not None:
                try:
                    element.click()
                    time.sleep(10)
                    return True
                except Exception as e:
                    print("Click Exception: "+str(e))
                    return False
        except Exception as exp:
            return False

    def TypeInto(self,text,element):
        try:
            element.send_keys(text)
        except Exception as exp:
            pass

    def ClearText(self,element):
        try:
            element.clear()
        except Exception as exp:
            print('Text Clear Failed!')
            pass

    def FindElementByName(self,elementName):
        try:
            element = self.browser.find_element_by_name(elementName)
            return element
        except Exception as exp:
            return None

    def FindElementById(self,elementId):
        try:
            element = self.browser.find_element_by_id(elementId)
            return element
        except Exception as exp:
            return None

    def FindElementByClassName(self,elementClassName):
        try:
            element = self.browser.find_element_by_css_selector(elementClassName)
            return element
        except Exception as exp:
            return None

    def FindElementByText(self,text):
        try:
            element = self.browser.find_element_by_link_text(text)
            return element
        except Exception as exp:
            return None

    def ExecuteScriptAndWait(self,code):
        try:
            self.browser.execute_script(code)
            time.sleep(7)
        except Exception as exp:
            pass

    def GetPageURL(self):
        try:
            return self.browser.current_url
        except Exception as exp:
            return None

    def Close(self):
        try:
            self.browser.close()
        except Exception as exp:
            print("Browser closing failed.")

    def scroll_to_pager_link(self):
        try:
            scrollto_pager_link = 'var el = document.getElementById("Y-N-ffs");el.scrollIntoView(true);'
            #scrollto_pager_link = 'var el = document.getElementsByClassName("pagination");el[0].scrollIntoView(true);'
            self.ExecuteScriptAndWait(scrollto_pager_link)
        except Exception as exp:
            print("Exception Inside page scroll. %s" % str(exp))






