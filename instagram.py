# - * - coding: UTF-8 - * -

import time
import os
import json
import re
from random import randint
from urllib import parse
from datetime import datetime, timedelta
from urlextract import URLExtract
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup
import gspread
from oauth2client.service_account import ServiceAccountCredentials


class Browser:

    def __init__(self,headless=False):
        try:
            executable_path = os.path.join(os.getcwd(), 'chromedriver')
            print(executable_path)
            options = webdriver.ChromeOptions()
            if headless:
                options.add_argument('headless')
            options.add_argument('window-size=1200x600')
            self.browser = webdriver.Chrome(executable_path=executable_path, chrome_options=options)
            self.browser.set_page_load_timeout(1000)
            self.browser.set_script_timeout(1000)
        except Exception as exp:
            print(str(exp))
            print("Webdriver open failed.")

    def OpenURL(self,url):
        try:
            self.browser.get(url)
            self.wait = ui.WebDriverWait(self.browser, 60)
            return True
        except Exception as exp:
            return False

    def get_url(self):
        return self.browser.current_url

    def get_query_parameters(self, url=None):
        if not url:
            url = self.get_url()
        query_def = parse.parse_qs(parse.urlparse(url).query)
        return query_def

    def build_url(self, base_url, query_params={}):
        q_params = []
        for k, v in query_params.items():
            q_params += [k+'='+v[0] if v else '']
        return base_url + '?' + ('&'.join(q_params))

    def wait_until_element_found_by_class(self, class_name):
        try:
            WebDriverWait(self.browser, 1000).until(EC.presence_of_element_located(
                (By.CLASS_NAME, class_name)))
        except Exception as exp:
            return None

    def GetPage(self):
        try:
            self.page = self.browser.page_source
            encodedStr = self.page.encode("ascii","xmlcharrefreplace")
            return encodedStr
        except Exception as exp:
            return None

    def ClickElement(self,element):
        try:
            if element is not None:
                try:
                    element.click()
                    time.sleep(10)
                    return True
                except Exception as e:
                    print("Click Exception: "+str(e))
                    return False
        except Exception as exp:
            return False

    def HitEnter(self, element):
        try:
            element.send_keys(u'\ue007')
        except Exception as exp:
            print(str(exp))

    def TypeInto(self,text,element):
        try:
            element.send_keys(text)
        except Exception as exp:
            pass

    def FindElementByName(self,elementName):
        try:
            element = self.browser.find_element_by_name(elementName)
            return element
        except Exception as exp:
            return None

    def FindElementById(self,elementId):
        try:
            element = self.browser.find_element_by_id(elementId)
            return element
        except Exception as exp:
            return None

    def FindElementByClassName(self,class_name):
        try:
            element = self.browser.find_elements_by_class_name(class_name)
            return element
        except Exception as exp:
            return None

    def FindElementByXPath(self, xpath):
        try:
            element = self.browser.find_element_by_xpath(xpath)
            return element
        except Exception as exp:
            return None

    def FindElementByText(self,text):
        try:
            element = self.browser.find_element_by_link_text(text)
            return element
        except Exception as exp:
            return None

    def ExecuteScriptAndWait(self,code):
        try:
            self.browser.execute_script(code)
            time.sleep(7)
        except Exception as exp:
            pass

    def GetPageURL(self):
        try:
            return self.browser.current_url
        except Exception as exp:
            return None

    def MoveToElement(self, element):
        actions = ActionChains(self.browser)
        actions.move_to_element(element).perform()

    def ScrollDown(self):
        html = self.browser.find_element_by_tag_name('html')
        html.send_keys(Keys.END)

    def Close(self):
        try:
            self.browser.close()
        except Exception as exp:
            print("Browser closing failed.")


class Scraper(object):
    def __init__(self, page):
        self.page = page
        self.soup = BeautifulSoup(self.page, features="html.parser")

    def scrape_comments(self):
        comment_users = []
        comments = {}
        comment_elements = self.soup.findAll('div', {'class': 'C4VMK'})
        for comment_element in comment_elements:
            commenter_anchors = comment_element.findAll('a', {'class': 'FPmhX'})
            if commenter_anchors:
                commenter_href = commenter_anchors[0].get('href')
                if commenter_href:
                    if not commenter_href in comment_users:
                        comment_users += [commenter_href]
                    comment_span_els = comment_element.findAll('span')
                    if comment_span_els:
                        comment_text = comment_span_els[0].text
                        if not commenter_href in comments:
                            comments[commenter_href] = [comment_text]
                        else:
                            comments[commenter_href] += [comment_text]
        return comment_users, comments

    def scrape_post_likes(self):
        liked_users = []
        liked_users_divs = self.soup.findAll('div', {'class': 'd7ByH'})
        for liked_users_div in liked_users_divs:
            liked_user_anchors = liked_users_div.findAll('a', {'class': 'FPmhX'})
            for a in liked_user_anchors:
                liked_users += [a.get('href')]
        return liked_users

    def scrape_post_urls(self):
        post_urls = []
        post_divs = self.soup.findAll('div', {'class': '_bz0w'})
        for post_div in post_divs:
            post_anchors = post_div.findAll('a')
            if post_anchors:
                href = post_anchors[0].get('href')
                if href:
                    post_urls += [href]
        return post_urls

    def parse_profile_info(self):
        profile = {}
        name_els = self.soup.findAll('h1', {'class': 'AC5d8'})
        if name_els:
            profile['name'] = name_els[0].text
        else:
            profile['name'] = ''

        followers_els = self.soup.findAll('li', {'class': 'Y8-fY'})
        if followers_els:
            for followers_el in followers_els:
                txt = followers_el.text
                if 'followers' in txt:
                    profile['followers'] = txt.replace('followers', '').strip()
                    break
        else:
            profile['followers'] = ''

        # print(profile['followers'])

        fullname_els = self.soup.findAll('h1', {'class': 'rhpdm'})
        if fullname_els:
            profile['fullname'] = fullname_els[0].text
        else:
            profile['fullname'] = ''

        profile['bio'] = ''
        bio_els = self.soup.findAll('div', {'class': '-vDIg'})
        if bio_els:
            bio_span_els = bio_els[0].findAll('span')
            if bio_span_els:
                bio = str(bio_span_els[0])
                bio_list = bio.split('<br/>')
                bio_list = [b.replace('<span>', '').replace('</span>', '') for b in bio_list]
                bio = '\n'.join(bio_list)
                profile['bio'] = bio

        return profile

    def parse_post_details(self):
        post_details = {}
        post_details['location'] = ""
        location_divs = self.soup.findAll('div', {'class': 'M30cS'})
        if location_divs:
            location_anchors = location_divs[0].findAll('a', {'class': 'O4GlU'})
            if location_anchors:
                post_details['location'] = location_anchors[0].text

        liked_count = None

        like_anchors = self.soup.findAll('a', {'class': 'zV_Nj'})
        if like_anchors:
            like_span = like_anchors[0].findAll('span')
            if like_span:
                liked_count = like_span[0].text

        post_details["liked_count"] = liked_count

        current_user = None
        u_anchors = self.soup.findAll('a', {'class': 'FPmhX'})
        if u_anchors:
            current_user = u_anchors[0].get('href')

        comment_count = None
        comment_lis = self.soup.findAll('li', {'class': 'gElp9 '})
        comment_count = 0
        for comment_li in comment_lis:
            c_anchors = comment_li.findAll('a', {'class': 'FPmhX'})
            if c_anchors:
                href = c_anchors[0].get('href')
                if href and href == current_user:
                    continue
                else:
                    comment_count += 1

        post_details["comment_count"] = comment_count

        def parse_prefix(line, fmt):
            cover = len(datetime.now().strftime(fmt))
            return datetime.strptime(line[:cover], fmt)

        post_details['post_time'] = None
        post_time_anchors = self.soup.findAll('a', {'class': 'c-Yi7'})
        if post_time_anchors:
            post_time_els = post_time_anchors[0].findAll('time')
            if post_time_els:
                post_details['post_time'] = parse_prefix(post_time_els[0]['datetime'], "%Y-%m-%dT%H:%M:%S").strftime('%Y-%m-%dT%H:%M:%S')

        return post_details


def perform_instagram_login(browser, username, password):
    print("Logging in...")
    LOGIN_URL = "https://www.instagram.com/accounts/login/"
    browser.OpenURL(url=LOGIN_URL)
    login_element = browser.FindElementByName("username")
    password_element = browser.FindElementByName("password")

    browser.TypeInto(username, login_element)
    browser.TypeInto(password, password_element)

    browser.HitEnter(password_element)
    browser.wait_until_element_found_by_class(class_name="logged-in")
    print("Logged In")


def scrape_all_likes(browser, url):
    print("Now opening URL: %s" % url)
    browser.OpenURL(url=url)
    browser.wait_until_element_found_by_class(class_name="_9eogI")
    print("Now click on the liked link")

    liked_users = []

    liked_element = browser.FindElementByClassName("zV_Nj")
    if liked_element:
        print("Found the liked element")
        div_el = browser.FindElementByClassName("EDfFK")
        if div_el:
            browser.MoveToElement(div_el[0])
        browser.ClickElement(liked_element[0])

        popup_window_divs = browser.FindElementByClassName("D7Y-g")
        if popup_window_divs:
            browser.ClickElement(popup_window_divs[0])

        liked_scroll_count = 100
        previous_liked_items = []
        while liked_scroll_count >= 0:
            print("Scrolling...")
            page = browser.GetPage()
            scraper = Scraper(page=page)
            post_likes = scraper.scrape_post_likes()
            if len(post_likes) != len(previous_liked_items):
                previous_liked_items = post_likes
            else:
                break
            browser.ScrollDown()
            liked_scroll_count -= 1

            #rtime = randint(1, 3)
            #time.sleep(rtime)

        page = browser.GetPage()
        scraper = Scraper(page=page)
        liked_users += scraper.scrape_post_likes()
        print("Closing the popup")

        popup_close_element = browser.FindElementByClassName("Gzt1P")
        if popup_close_element:
            browser.ClickElement(popup_close_element[0])
            print("Popup closed!")
        else:
            print("Popup not found!")
    else:
        print("Liked element not found!")

    return liked_users


def scrape_all_comments(browser, url):
    print("Now opening URL: %s" % url)
    browser.OpenURL(url=url)
    browser.wait_until_element_found_by_class(class_name="_9eogI")
    # print("Now click on the liked link")

    commented_users = []
    comments = {}

    comment_scroll_count = 100
    while comment_scroll_count >= 0:
        comment_element = browser.FindElementByClassName("Z4IfV")
        if comment_element:
            browser.MoveToElement(comment_element[0])
            browser.ClickElement(comment_element[0])
        else:
            print("Comment element not found!")
            break
        comment_scroll_count -= 1

        # rtime = randint(1, 3)
        # time.sleep(rtime)

    els = browser.FindElementByClassName("C4VMK")
    for el in els:
        ce = el.find_element_by_tag_name('a')
        # print(ce.get_property('attributes')[0].get('textContent'))
        href = ce.get_attribute('href').replace("https://www.instagram.com", "") # https://www.instagram.com/toddlinacrossamerica/
        text = ce.text
        # print(href)
        if href not in commented_users:
            commented_users += [href]
        span_element = el.find_element_by_tag_name('span')
        # print(span_element)
        if span_element:
            comment = span_element.text
            # print(comment)
            if href not in comments:
                comments[href] = [comment]
            else:
                comments[href] += [comment]
        # print(comments)

    return commented_users, comments


def scrape_profiles(browser, url):
    users = []

    liked_users = scrape_all_likes(browser, url)

    users += liked_users

    time.sleep(1)

    commented_users, comments = scrape_all_comments(browser, url)

    users += commented_users

    users = list(set(users))

    # print("Total %s liked users" % len(liked_users))

    print("Total %s comments" % len(comments))

    print("Total %s users found!" % len(users))

    print("Now saving profiles to json for later use")

    d = {}
    for pl in users:
        if pl in commented_users and pl in liked_users:
            d['https://www.instagram.com' + pl] = {
                'liked': True,
                'comment': True,
                'comments': comments.get(pl, []),
                'profile': False
            }
        elif pl in liked_users and pl not in commented_users:
            d['https://www.instagram.com' + pl] = {
                'liked': True,
                'comment': False,
                'comments': [],
                'profile': False
            }
        elif pl not in liked_users and pl in commented_users:
            d['https://www.instagram.com' + pl] = {
                'liked': False,
                'comment': True,
                'comments': comments.get(pl, []),
                'profile': False
            }

    file_name = 'instagram_users.json'
    with open(file_name, 'w') as outfile:
        json.dump(d, outfile)

    print("likers and commenters instagram links are saved in %s" % file_name)

    # print("Closing browser")
    # browser.Close()


def scrape_profile_details(browser):

    MINIMUM_FOLLOWER_COUNT = 0

    basic_profiles = {}
    with open("instagram_users.json", "r") as file:
        content = file.read()
        if content:
            basic_profiles = json.loads(content)
        else:
            basic_profiles = {}

    for profile_url, basic_profile_info in basic_profiles.items():
        if basic_profile_info.get('profile'):
            continue
        print("Scraping details for: %s" % profile_url)
        browser.OpenURL(url=profile_url)
        browser.wait_until_element_found_by_class(class_name="LWmhU")
        print("URL opened")
        # follow_btn = browser.FindElementByClassName("BY3EC")
        # if follow_btn:
        #     print("Profile skipping")
        #     continue

        page = browser.GetPage()
        scraper = Scraper(page=page)
        profile = scraper.parse_profile_info() or {}
        print("Profile scraped")

        followers_count = profile.get("followers", '')
        try:
            followers_count = followers_count.replace(',', '').strip()
            if followers_count.endswith('k'):
                followers_count = followers_count.replace('k', '').strip()
                followers_count = int(float(followers_count) * 1000)
            else:
                followers_count = int(followers_count)
            # followers_count = float(followers_count.strip())
        except Exception as exp:
            print(str(exp))
            followers_count = 0

        followers_count = int(followers_count)

        print(followers_count)

        basic_profile_info['n_followers'] = followers_count

        print("Now extract post links")
        if followers_count >= MINIMUM_FOLLOWER_COUNT: # Only users who have followers count greater than 1000 will need to consider for further processing
            scroll_max_limit = 2
            while scroll_max_limit > 0:
                browser.ScrollDown()
                rtime = randint(1, 3)
                time.sleep(rtime)

                scroll_max_limit -= 1
        page = browser.GetPage()
        # with open("post_details.html", "w") as file:
        #     file.write(str(page))
        post_urls = []
        els = browser.FindElementByClassName("_bz0w")
        for el in els:
            ce = el.find_element_by_tag_name('a')
            post_urls += [ce.get_property('attributes')[0].get('textContent')]

        # scraper = Scraper(page=page)
        # post_urls = scraper.scrape_post_urls()
        print(post_urls)
        # return
        first_post = post_urls[0] if post_urls else None

        profile["post_details_list"] = []

        if first_post:

            browser.OpenURL("https://www.instagram.com" + first_post)
            comment_element = browser.FindElementByClassName("Z4IfV")
            if comment_element:
                browser.ClickElement(comment_element[0])

            page = browser.GetPage()
            scraper = Scraper(page=page)

            profile["first_post"] = scraper.parse_post_details()

            post_from, post_till = datetime.now() - timedelta(days=2), datetime.now() - timedelta(days=12)

            print(post_from)
            print(post_till)

            post_time = profile["first_post"]['post_time']

            if post_time:
                post_time = datetime.strptime(post_time, "%Y-%m-%dT%H:%M:%S")

            if post_time and post_from >= post_time >= post_till:
                posts_details_list = [profile["first_post"]]
            else:
                posts_details_list = []

            if followers_count >= MINIMUM_FOLLOWER_COUNT:
                print("Found %s posts. Now visiting each one" % str(len(post_urls)))

                engagement_list = []
                n_followers = followers_count

                post_urls = post_urls[1:13]
                for post_url in post_urls:
                    print("Entering post: https://www.instagram.com%s" % post_url)
                    browser.OpenURL("https://www.instagram.com" + post_url)
                    comment_element = browser.FindElementByClassName("Z4IfV")
                    if comment_element:
                        browser.ClickElement(comment_element[0])

                    page = browser.GetPage()
                    scraper = Scraper(page=page)

                    post_info = scraper.parse_post_details()

                    print(post_info)

                    post_time = post_info['post_time']

                    print("Post Date: %s" % post_time)

                    if post_time:
                        post_time = datetime.strptime(post_time, "%Y-%m-%dT%H:%M:%S")

                    if post_time and post_from >= post_time >= post_till:
                        posts_details_list += [post_info]
                        n_liked = post_info['liked_count'] or 0
                        try:
                            if n_liked:
                                n_liked = n_liked.replace(',', '').strip()
                            n_liked = int(n_liked)
                        except Exception as exp:
                            pass

                        n_comments = post_info['comment_count'] or 0
                        try:
                            if n_comments:
                                n_comments = n_comments.replace(',', '').strip()
                            n_comments = int(n_comments)
                        except Exception as exp:
                            pass

                        engmnt = (n_liked + (n_comments * 5)) / n_followers
                        engagement_list += [engmnt]

                    if post_time and post_time < post_till:
                        break

                    rtime = randint(0, 2)
                    time.sleep(rtime)

                if engagement_list:
                    avg_engagement = sum(engagement_list) / len(engagement_list)
                    profile["engagement"] = avg_engagement
                else:
                    profile["engagement"] = 0.0

                print("Now calculating the engagement percentage")

            profile["post_details_list"] = posts_details_list

        rtime = randint(1, 2)
        print("Sleeping for %s seconds" % rtime)
        time.sleep(rtime)

        print(profile)

        basic_profile_info['profile'] = profile

        basic_profiles[profile_url] = basic_profile_info

        print("Saving profile...")
        file_name = 'instagram_users.json'
        with open(file_name, 'w') as out_file:
            json.dump(basic_profiles, out_file)
        print("Saving profile done!")


def save_profile_to_google_sheets(url, credentials="client_secrets.json"):
    # https://github.com/burnash/gspread
    # https://www.twilio.com/blog/2017/02/an-easy-way-to-read-and-write-to-a-google-spreadsheet-in-python.html
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(credentials, scope)
    client = gspread.authorize(creds)

    sheet_headers = ["Instagram Username", "Data Scraping Date", "Total Followers", "Comments", "Bio", "Engagement", "Email", "Link", "Location"]
    data_rows = []

    now_date = datetime.now().strftime('%d/%m/%Y %H:%M')

    profile_data = {}
    file_name = 'instagram_users.json'
    with open(file_name, "r") as pfile:
        content = pfile.read()
        """
            {
                "https://www.instagram.com/peteandjordan/":
                {  
                   'first_post':{  
                      'location':'Lewisburg, Pennsylvania',
                      'liked_count':'231',
                      'comment_count':24,
                      'post_time':'2018-11-15T18:19:21'
                   },
                   'followers':'3,589',
                   'fullname':'Pete + Jordan Jones',
                   'engagement':0.0010605638440689988,
                   'name':'peteandjordan',
                   'bio':'💛 Newlyweds + Believers\n🚌 <a href="/explore/tags/fulltimervers/">#fulltimeRVers</a> working on our first <a href="/explore/tags/rvrenovation/">#rvrenovation</a>\n🌿 Marketing + Biz Growth Coaches for <a href="/explore/tags/weddingpros/">#weddingpros</a> + creatives',
                   'post_details_list':[  ]
                }
            }
        """
        profile_data = json.loads(content)
        print(len(profile_data))
        # print(profile_data)
        pc = 0
        for profile_url, profile_info in profile_data.items():
            # print(profile_info)
            # print(profile_url)

            profile = profile_info.get('profile')

            if type(profile) is bool:
                pc += 1
                continue

            data_row = []
            data_row += [profile.get('name', '')]
            data_row += [now_date]
            data_row += [profile.get('followers', '')]
            comments = "|".join(profile_info.get("comments", []))
            data_row += [comments]
            data_row += [profile.get("bio", "")]
            data_row += [profile.get("engagement", "")]

            match = re.search(r'[\w\.-]+@[\w\.-]+', profile.get("bio", ""))
            if match:
                email = match.group(0)
            else:
                email = ""

            data_row += [email]

            u_url = ""

            extractor = URLExtract()
            urls = extractor.find_urls(profile.get("bio", ""))
            if urls:
                u_url = ", ".join(urls)

            data_row += [u_url]
            location = ""
            first_post = profile.get("first_post")
            if first_post:
                location = first_post.get("location", "")
            data_row += [location]

            # print(data_row)
            data_rows += [data_row]

    print(pc)

    # Open a worksheet from spreadsheet with one shot
    print("URL: " + url)
    worksheet = client.open(url).sheet1
    
    sheet_index = 1

    worksheet.insert_row(sheet_headers, sheet_index)

    sheet_index += 1

    print(len(data_rows))

    cnt = 1
    start_save = True
    for data_row in data_rows:
        if start_save:
            worksheet.insert_row(data_row, sheet_index)

            time.sleep(1)
            if cnt % 90 == 0:
                time.sleep(2)
        # if data_row[0] == 'my3ratbagz3614':
        #     start_save = True
        cnt += 1
        if cnt == 500:
            time.sleep(20)

    print("Saving done!")


if __name__ == "__main__":
    # https://docs.google.com/document/d/17d-ORAdWwDS3Jt5JR2yHIM60zxpmIkb6_i3DwBfWCGs/edit
    URL = "https://www.instagram.com/p/BqpSo62gAJr/"
    INSTA_LOGIN = "codenginebd2@gmail.com"
    INSTA_PASSWORD = "lapsso065"

    browser = Browser(headless=True)
    perform_instagram_login(browser=browser, username=INSTA_LOGIN, password=INSTA_PASSWORD)

    scrape_profiles(browser=browser, url=URL)
    #
    print("Scraping profiles completed")

    scrape_profile_details(browser)
    #
    print("Scraping profile details completed")

    print("Saving to google sheet")
    url = "Instagram Profiles Spreadsheet"
    save_profile_to_google_sheets("InstaGram")

    print("Saving completed.")
    # print("Closing browser")
    # browser.Close()
    # print("Browser closed")





